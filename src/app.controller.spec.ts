import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return a warm welcome message', () => {
      expect(appController.getHello()).toHaveProperty('statusCode', 200);
      expect(appController.getHello()).toHaveProperty(
        'message',
        'Welcome to the Nest Todo App',
      );
    });
  });
});
