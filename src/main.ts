import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';

const SERVER_PORT = process.env.SERVER_PORT || 5000;
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  const config = new DocumentBuilder()
    .setTitle('Todo API Documentation')
    .setDescription('Todo App is simple and awesome app to organize your tasks')
    .setContact(
      'NIYODUSENGA Clement',
      'https://www.linkedin.com/in/niyodusenga-clement',
      'clementmistico@gmail.com',
    )
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'Authorization',
    )
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/doc', app, document, {
    customSiteTitle: 'Todo API Documentation',
  });
  await app.listen(SERVER_PORT);
}
bootstrap();
