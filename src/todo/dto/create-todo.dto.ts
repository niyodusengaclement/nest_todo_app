import { IsNotEmpty, MinLength, Length, IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateTodoDto {
  @ApiProperty()
  @IsNotEmpty()
  @Length(10, 15, {
    message: 'Title must not be below 10 and not exceeds 15 characters',
  })
  readonly title: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(10, { message: 'Description should have more than 10 characters' })
  readonly description: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(['HIGH', 'MEDIUM', 'LOW'], {
    message: 'Priority must be either HIGH, MEDIUM or LOW',
  })
  readonly priority: string;
  createdBy: number;
}
