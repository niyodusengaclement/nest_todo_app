import { UserEntity } from 'src/user/entities/user.entity';
import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';
import { BaseEntity } from '../../shared/models/base.entity';

@Entity({ name: 'todos' })
export class TodosEntity extends BaseEntity {
  @Column()
  title: string;

  @Column()
  description: string;

  @Column({ type: 'enum', enum: ['LOW', 'MEDIUM', 'HIGH'] })
  priority: string;

  @ManyToOne(() => UserEntity)
  @JoinColumn({ name: 'createdBy' })
  owner?: UserEntity;
}
