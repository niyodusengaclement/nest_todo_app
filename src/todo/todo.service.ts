import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserDto } from 'src/user/dto/user.dto';
import { UserService } from 'src/user/user.service';
import { Repository } from 'typeorm';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { TodosEntity } from './entities/todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(TodosEntity)
    private TodosRepository: Repository<TodosEntity>,
    private userService: UserService,
  ) {}
  async create({ id }: UserDto, createTodoDto: CreateTodoDto) {
    const owner = await this.userService.findUser(id);
    const myTodo: TodosEntity = this.TodosRepository.create({
      owner,
      ...createTodoDto,
    });
    const todo = await this.TodosRepository.save(myTodo);
    return { message: 'Todo successfully created', data: todo };
  }

  async findAll() {
    const todos = await this.TodosRepository.find();
    return { message: 'Todos successfully found', data: todos };
  }

  async findOne(id: number) {
    const todo = await this.TodosRepository.findOne(
      { id },
      { relations: ['owner'] },
    );
    if (!todo) {
      throw new NotFoundException('Todo not found');
    }
    delete todo?.owner?.password;
    return { message: 'Todo successfully found', data: todo };
  }

  async update(id: number, updateTodoDto: UpdateTodoDto) {
    const todo = await this.TodosRepository.findOne({ id });
    if (!todo) {
      throw new NotFoundException('Todo not found');
    }
    await this.TodosRepository.update({ id }, updateTodoDto);
    return { message: 'Todo successfully updated', data: todo };
  }

  async remove(id: number) {
    const todo = await this.TodosRepository.findOne({ id });
    if (!todo) {
      throw new NotFoundException('Todo not found');
    }
    await this.TodosRepository.delete({ id });
    return { message: 'Todo successfully deleted', data: todo };
  }
}
