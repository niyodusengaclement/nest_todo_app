import { IsNotEmpty, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(5, {
    message: 'Title must not be below 10 characters',
  })
  readonly name: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(4, { message: 'Your username is too short' })
  readonly username: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(6, { message: 'Password must have more than 6 characters long' })
  readonly password: string;
}
