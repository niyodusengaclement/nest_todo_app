export class UserDto {
  readonly id: number;
  readonly name: string;
  readonly username: string;
}
