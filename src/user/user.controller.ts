import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
@ApiTags('Auth')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('register')
  register(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @Post('login')
  login(@Body() loginUserDto: LoginUserDto) {
    return this.userService.findByLogin(loginUserDto);
  }

  @Get('users')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('Authorization')
  findAll() {
    return this.userService.findAll();
  }

  @Get('users/:id')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('Authorization')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(+id);
  }

  @Put('users/:id')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('Authorization')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(+id, updateUserDto);
  }

  @Delete('users/:id')
  @UseGuards(AuthGuard())
  @ApiBearerAuth('Authorization')
  remove(@Param('id') id: string) {
    return this.userService.remove(+id);
  }
}
