import {
  ConflictException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { UserDto } from './dto/user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private UserRepository: Repository<UserEntity>,
    private readonly jwtService: JwtService,
  ) {}
  async create(createUserDto: CreateUserDto) {
    const user = await this.UserRepository.findOne({
      where: { username: createUserDto.username },
    });
    if (user) {
      throw new ConflictException(
        `Username ${createUserDto.username} already exists`,
      );
    }
    const userData = await this.UserRepository.create(createUserDto);
    const res = await this.UserRepository.save(userData);
    delete res.password;
    return { message: 'user successfully created', data: res };
  }

  async findAll() {
    const users = await this.UserRepository.find({
      select: ['name', 'username', 'id'],
    });
    return { message: 'users successfully found', data: users };
  }

  async findOne(id: number) {
    const user = await this.UserRepository.findOne({
      where: { id },
      select: ['name', 'username', 'id'],
    });
    if (!user) {
      throw new NotFoundException('user not found');
    }
    return { message: 'user successfully found', data: user };
  }

  async findUser(id: number) {
    return await this.UserRepository.findOne({
      where: { id },
      select: ['name', 'username', 'id'],
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.UserRepository.findOne({ id });
    if (!user) {
      throw new NotFoundException('user not found');
    }
    await this.UserRepository.update({ id }, updateUserDto);
    return { message: 'user successfully updated', data: user };
  }

  async remove(id: number) {
    const user = await this.UserRepository.findOne({ id });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    await this.UserRepository.delete({ id });
    return { message: 'user successfully deleted', data: user };
  }

  async findByLogin({ username, password }: LoginUserDto) {
    const user = await this.UserRepository.findOne({ where: { username } });
    if (!user) {
      throw new UnauthorizedException('Invalid username or password');
    }
    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      throw new UnauthorizedException('Invalid username or password');
    }
    const token = this.generateToken(user);
    return { message: 'You are logged in successfully', data: token };
  }

  private generateToken({ username, id }: any): any {
    const token = this.jwtService.sign({ username, id });
    return { token };
  }
}
